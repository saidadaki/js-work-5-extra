function tinhTienThue(){
    var individual = document.getElementById("individual").value;
    var salary = document.getElementById("salary").value * 1;
    var person = document.getElementById("person").value * 1;
    if (salary < 0 || person < 0){
        alert("Du lieu khong hop le");
    }
    if(Number.isInteger(person) == false){
        alert("Du lieu khong hop le");
    }
    var tax = salary - 4 - person*1.6;
    var realTax = 0;
    if(tax<0){
        alert("Tiền thu không hợp lệ");
        document.getElementById("result__3").innerHTML = `Họ và tên: ${individual}, Tiền thuế thu nhập cá nhân: 0 VND`;
    }
    else if(tax>0 && tax <= 60){
        var realTax = (tax*5)/100;
    }else if(tax>60 && tax<=120){
        var realTax = (tax*10)/100;
    }else if(tax>120 && tax<=210){
        var realTax = (tax*15)/100;
    }else if(tax>210 && tax<=384){
        var realTax = (tax*20)/100;
    }else if(tax>384 && tax<=624){
        var realTax = (tax*25)/100;
    }else if(tax>624 && tax<=960){
        var realTax = (tax*30)/100;
    }else{
        var realTax = (tax*35)/100;
    }
    document.getElementById("result__3").innerHTML = `Họ và tên: ${individual}, Tiền thuế thu nhập cá nhân: ${(realTax*1000000).toLocaleString("en-US")} VND`;
    document.getElementById("result__3").style.display = "inline-block";
}


function tinhTienCap(){
    var khachHang = document.getElementById("code").value;
    var kenhCaoCap = document.getElementById("channel").value * 1;
    var ketNoi = document.getElementById("connection").value * 1;
    if(kenhCaoCap < 0){
        alert("Du lieu khong hop le");
    }

    var tienNhaDan = 4.5 + 20.5 + kenhCaoCap*7.5;
    
    switch(document.getElementById("customer").value){
        case "A":
            document.getElementById("result__4").innerHTML = `Mã khách hàng: ${khachHang}, Tiền cáp: $${tienNhaDan.toFixed(2)}`;
            break;
        case "B":
            if(ketNoi<=0){
                alert("Du lieu khong hop le");
            }else if(ketNoi<=10 && ketNoi>0){
                var phiDichVu = 75;
            }else if(ketNoi >10){
                var phiDichVu = 75+ (ketNoi -10)*5;
            }
        
            var tienDoanhNghiep = 15 + phiDichVu + kenhCaoCap*50;
            document.getElementById("result__4").innerHTML = `Mã khách hàng: ${khachHang}, Tiền cáp: $${tienDoanhNghiep.toFixed(2)}`;
            break;
    }
    document.getElementById("result__4").style.display = "inline-block";
}